package database;
import database.example.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/social_network", "postgres", "12345678");
            Session session = new Session(connection);

            User user = new User("Claudiu", "Neamtu", "test", "test");

            System.out.println(user.firstName.getValue());
            System.out.println(user.lastName.getValue());

            user.save(connection);

            user.remove(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
